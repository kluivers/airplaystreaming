# AirPlay audio streaming demo

by Joris Kluivers [@kluivers on Twitter](http://twitter.com/kluivers).

Blog post:
[Per application AirPlay streaming in Mountain Lion][blog-post]

## Audio
Demo audio used:
[Perfect Tomorrow by Mokhov][audio]

[blog-post]: http://joris.kluivers.nl/blog/2012/07/25/per-application-airplay-in-mountain-lion/
[audio]: http://www.beatpick.com/artists/Mokhov/perfect_tomorrow